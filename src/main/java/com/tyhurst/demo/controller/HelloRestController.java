package com.tyhurst.demo.controller;

import com.tyhurst.demo.entities.Greeting;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloRestController {

    @GetMapping("/rest-greeting")
    public Greeting greet(
            @RequestParam(required = false, defaultValue = "World") String name
    ) {
        return(new Greeting(String.format("Hello, %s!", name)));
    }
}
