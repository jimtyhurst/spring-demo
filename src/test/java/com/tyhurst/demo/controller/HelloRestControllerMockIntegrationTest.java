package com.tyhurst.demo.controller;

import com.tyhurst.demo.entities.Greeting;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

// These annotations specify the test environment, which starts
// a TomcatWebServer to provide the context for testing the
// RestController with REST calls managed by a TestRestTemplate.
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class HelloRestControllerMockIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    // Demonstrates 'getForEntity'.
    @Test
    public void greetWithoutName() {
        ResponseEntity<Greeting> entity = restTemplate.getForEntity("/rest-greeting", Greeting.class);
        assertEquals(HttpStatus.OK, entity.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, entity.getHeaders().getContentType());
        Greeting rs = entity.getBody();
        assertNotNull(rs);
        assertEquals("Hello, World!", rs.getMessage());
    }

    // Demonstrates 'getForObject'.
    @Test
    public void greetWithName() {
        String expectedName = "Riley";
        Greeting rs = restTemplate.getForObject(String.format("/rest-greeting?name=%s", expectedName), Greeting.class);
        assertNotNull(rs);
        assertEquals(String.format("Hello, %s!", expectedName), rs.getMessage());
    }

}
