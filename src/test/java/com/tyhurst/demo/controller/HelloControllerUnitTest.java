package com.tyhurst.demo.controller;

import org.junit.jupiter.api.Test;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HelloControllerUnitTest {

    @Test
    public void sayHello() {
        HelloController sut = new HelloController();
        Model model = new BindingAwareModelMap();
        String actualRs = sut.sayHello("World", model);
        assertEquals("hello", actualRs);
        assertEquals("World", model.asMap().get("user"));
    }
}
