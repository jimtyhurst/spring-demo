package com.tyhurst.demo.controller;

import com.tyhurst.demo.entities.Greeting;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HelloRestControllerUnitTest {

    @Test
    public void greetWithName() {
        String expectedName = "Riley";
        HelloRestController sut = new HelloRestController();
        Greeting actualGreeting = sut.greet(expectedName);
        assertEquals(String.format("Hello, %s!", expectedName), actualGreeting.getMessage());
    }
}
