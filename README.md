# Spring Boot Demo app

[![pipeline status](https://gitlab.com/jimtyhurst/spring-demo/badges/master/pipeline.svg)](https://gitlab.com/jimtyhurst/spring-demo/-/commits/master)
  [![coverage report](https://gitlab.com/jimtyhurst/spring-demo/badges/master/coverage.svg)](https://gitlab.com/jimtyhurst/spring-demo/-/commits/master)

This is a simple "Hello, World!" app to demonstrate some of the features of [Spring Boot](https://spring.io/projects/spring-boot).

I also used this project as a way to explore the functionality provided by [GitLab](https://about.gitlab.com/) as a product that claims to provide [support for the entire DevOps lifecycle](https://about.gitlab.com/stages-devops-lifecycle/).


## Development environment
The initial template for the project was generated by the [Spring Initializr](https://start.spring.io/). The project uses:

* [JDK 11](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html): Java™ Platform, Standard Edition Development Kit (JDK™). 
* [Spring Boot 2](https://spring.io/projects/spring-boot) for Spring-based applications that run stand-alone, without being deployed to an app server.
* [Gradle 6](https://gradle.org/) for managing dependencies and builds.
* [JUnit 5](https://junit.org/junit5/) for unit testing.
* [SpotBugs](https://spotbugs.github.io/) for static code analysis.

### Installation
1. Clone this repository.
2. This application has been tested with Java 11.
3. You do _not_ need to install Gradle. From the top-level directory of this application directory, you can run the following shell commands to build, test, or run this sample web application. 

### To build ...
`./gradlew build`

### To test ...
`./gradlew test`

### To run the web app ...
From a command line:<br>
`./gradlew bootRun`

then navigate in a web browser to the home page at `http://localhost:8080/`

After the application has been built, it can also be started from the command line with:<br>
`java -jar build/libs/demo-0.0.1-SNAPSHOT.jar`

### To stop the web app ...
In the same shell where you started the app, type `<CTRL-C>` to stop the server.


## License
MIT License

Copyright (c) 2020 Jim Tyhurst

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
